package com.example.singlescreenapp

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var vieww: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vieww = inflater.inflate(R.layout.fragment_list, container, false)

        return vieww
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val btnAddCompany: Button = vieww.findViewById(R.id.btnAddCompany)

        recyclerView = vieww.findViewById(R.id.list)
        recyclerView.setHasFixedSize(true)

        recyclerView.layoutManager = LinearLayoutManager(this.activity)

        val dbHelper = CompanyEntry.CompaniesDbHelper(context!!)
        val companies = dbHelper.getAllCompanies()

        btnAddCompany.setOnClickListener {
            startActivity(Intent(context, AddCompanyActivity::class.java))
        }

        recyclerView.adapter = CompaniesAdapter(this.context!!, companies)

    }


}
