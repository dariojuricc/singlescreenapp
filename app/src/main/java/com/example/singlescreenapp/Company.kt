package com.example.singlescreenapp

data class Company(
    val name: String,
    val description: String
)