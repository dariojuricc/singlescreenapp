package com.example.singlescreenapp

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class MainActivity : AppCompatActivity(), CompaniesAdapter.ItemClicked {

    private lateinit var tvName: TextView
    private lateinit var tvDescription: TextView
    private lateinit var manager: FragmentManager
    private lateinit var listFragment: Fragment
    private lateinit var detailFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvName = findViewById(R.id.tvNameDetail)
        tvDescription = findViewById(R.id.tvDescriptionDetail)

        manager = supportFragmentManager
        listFragment = manager.findFragmentById(R.id.listFragment)!!
        detailFragment = manager.findFragmentById(R.id.detailFrag)!!
        manager.beginTransaction().hide(detailFragment).commit()
    }

    override fun onItemClicked(index: Int) {
        val dbHelper = CompanyEntry.CompaniesDbHelper(this)
        val companies = dbHelper.getAllCompanies()
        manager.beginTransaction()
            .show(detailFragment)
            .hide(listFragment)
            .addToBackStack(null)
            .commit()
        tvName.text = companies[index].name
        tvDescription.text = companies[index].description
    }
}
