package com.example.singlescreenapp

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_layout.view.*

class CompaniesAdapter(context: Context, val data: ArrayList<Company>):
    RecyclerView.Adapter<CompaniesAdapter.ViewHolder>() {

    val activity: ItemClicked = context as ItemClicked

    interface ItemClicked {
        fun onItemClicked(index: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_layout, parent, false)

        return ViewHolder(view, activity, data)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tag = data[position]

        holder.tvName.text = data[position].name
        holder.tvDescription.text = data[position].description
    }

    class ViewHolder(private val view: View, val activity: ItemClicked, val data: ArrayList<Company>): RecyclerView.ViewHolder(view) {

        val tvName: TextView = view.tvName
        val tvDescription: TextView = view.tvDescription

        init {
            view.setOnClickListener {

                activity.onItemClicked(data.indexOf(view.getTag()))
            }
        }
    }

}