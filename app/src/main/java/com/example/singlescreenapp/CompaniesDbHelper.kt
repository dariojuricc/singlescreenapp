package com.example.singlescreenapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns


object CompanyEntry : BaseColumns {
    const val KEY_ROWID = "_ID"
    const val KEY_NAME = "name"
    const val KEY_DESCRIPTION = "description"
    const val DATABASE_NAME = "CompaniesDB"
    const val DATABASE_TABLE = "CompaniesTable"
    const val DATABASE_VERSION = 1

    class CompaniesDbHelper(context: Context) :
        SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase?) {
            db!!.execSQL(SQL_CREATE_ENTRIES)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            TODO("Not yet implemented")
        }

        fun createCompany(name: String, description: String): Long {
            val db = writableDatabase

            val values = ContentValues().apply {
                put(KEY_NAME, name)
                put(KEY_DESCRIPTION, description)
            }
            return db.insert(DATABASE_TABLE, null, values).toLong()
        }

        fun getAllCompanies(): ArrayList<Company> {
            val db = readableDatabase

            val projection = arrayOf(BaseColumns._ID, KEY_NAME, KEY_DESCRIPTION)

            val sortOrder = "${KEY_NAME} ASC"

            val cursor = db.query(
                DATABASE_TABLE,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
            )

            val entries = ArrayList<Company>()
            with(cursor) {
                while(moveToNext()) {
                    val name = getString(getColumnIndexOrThrow(KEY_NAME))
                    val description = getString(getColumnIndexOrThrow(KEY_DESCRIPTION))
                    entries.add(Company(name, description))
                }
            }
            cursor.close()
            return entries
        }

        companion object {
            private const val SQL_CREATE_ENTRIES =
                "CREATE TABLE $DATABASE_TABLE (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "$KEY_NAME TEXT," +
                        "$KEY_DESCRIPTION TEXT)"
            private const val SQL_GET_ENTRIES =
                ""
        }
    }
}
