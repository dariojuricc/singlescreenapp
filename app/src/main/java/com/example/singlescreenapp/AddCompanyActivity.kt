package com.example.singlescreenapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class AddCompanyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_company)

        val etName: EditText = findViewById(R.id.etName)
        val etDescription: EditText = findViewById(R.id.etDescription)
        val btnAdd: Button = findViewById(R.id.btnSave)

        btnAdd.setOnClickListener {
            if(etName.text.isEmpty() || etDescription.text.isEmpty()) {
                Toast.makeText(this, "All Fields are required", Toast.LENGTH_SHORT).show()
            } else {
                CompanyEntry.CompaniesDbHelper(this)
                    .createCompany(etName.text.toString(), etDescription.text.toString())
                startActivity(Intent(this, MainActivity::class.java))
            }
        }

    }
}
